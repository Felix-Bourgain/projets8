package com.barthelemot.kylian.projets8.entity;

//Entité représentant une fenêtre
public class Window {

    private int id;
    private String nomPiece;
    private int ouverture;

    public Window(int id, String nomPiece, int ouverture) {
        this.id = id;
        this.nomPiece = nomPiece;
        this.ouverture = ouverture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomPiece() {
        return nomPiece;
    }

    public void setNomPiece(String nomPiece) {
        this.nomPiece = nomPiece;
    }

    public int getOuverture() {
        return ouverture;
    }

    public void setOuverture(int ouverture) {
        this.ouverture = ouverture;
    }
}
