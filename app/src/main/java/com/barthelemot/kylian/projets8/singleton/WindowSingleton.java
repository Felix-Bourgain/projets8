package com.barthelemot.kylian.projets8.singleton;

import com.barthelemot.kylian.projets8.entity.Window;

import java.util.ArrayList;

//Singleton pour la gestion des fenêtres
public class WindowSingleton {
    private static final WindowSingleton instance = new WindowSingleton();

    private ArrayList<Window> windowList = new ArrayList<>();

    private Window currentWindow;

    public static WindowSingleton getInstance() {
        return instance;
    }

    public ArrayList<Window> getListWindow() {
        return windowList;
    }

    public void setWindowList(ArrayList<Window> windowList) {
        this.windowList = windowList;
    }

    public Window getCurrentWindow() {
        return currentWindow;
    }

    public void setCurrentWindow(Window currentWindow) {
        this.currentWindow = currentWindow;
    }

    public int getCountWindow() {
        return windowList.size();
    }

    public Window getWindow(int index) {
        return windowList.get(index);
    }
}
