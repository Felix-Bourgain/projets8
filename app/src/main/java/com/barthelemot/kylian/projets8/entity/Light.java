package com.barthelemot.kylian.projets8.entity;

//Entité représentant une lumière
public class Light {

    private int id;
    private String nomPiece;
    private int intensity;
    private int r,g,b; //Non utilisé pour cause de problème quant aux led RGB
    // Un simple tableau de 3 entiers initialiement utilisé
    // mais pour simplifier l'envoi vers FireBase qui ne comprend pas les tableaux,
    // création de 3 entiers r,g,b.

    public Light(int id, String nomPiece, int intensity, int r, int g, int b) {
        this.id = id;
        this.nomPiece = nomPiece;
        this.intensity = intensity;
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomPiece() {
        return nomPiece;
    }

    public void setNomPiece(String nomPiece) {
        this.nomPiece = nomPiece;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    //Non utilisé pour cause de problème quant aux led RGB
    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
