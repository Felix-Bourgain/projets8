package com.barthelemot.kylian.projets8.singleton;

import com.barthelemot.kylian.projets8.entity.Thermo;

import java.util.ArrayList;

//Singleton pour la gestion des radiateurs
public class ThermoSingleton {
    private static final ThermoSingleton instance = new ThermoSingleton();

    private ArrayList<Thermo> thermoList = new ArrayList<>();

    private Thermo currentThermo;

    public static ThermoSingleton getInstance() {
        return instance;
    }

    public ArrayList<Thermo> getThermoList() {
        return thermoList;
    }

    public void setThermoList(ArrayList<Thermo> thermoList) {
        this.thermoList = thermoList;
    }

    public Thermo getCurrentThermo() {
        return currentThermo;
    }

    public void setCurrentThermo(Thermo currentThermo) {
        this.currentThermo = currentThermo;
    }

    public int getCountThermo() {
        return thermoList.size();
    }

    public Thermo getThermo(int index) {
        return thermoList.get(index);
    }
}
