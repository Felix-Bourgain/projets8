package com.barthelemot.kylian.projets8.singleton;

import com.barthelemot.kylian.projets8.entity.Light;

import java.util.ArrayList;

//Singleton pour la gestion des lumières
public class LightSingleton {

    private static final LightSingleton instance = new LightSingleton();

    private ArrayList<Light> lightList = new ArrayList<>();

    private Light currentLight;

    public static LightSingleton getInstance() {
        return instance;
    }

    public ArrayList<Light> getLightList() {
        return lightList;
    }

    public void setLightList(ArrayList<Light> lightList) {
        this.lightList = lightList;
    }

    public Light getCurrentLight() {
        return currentLight;
    }

    public void setCurrentLight(Light currentLight) {
        this.currentLight = currentLight;
    }

    public int getCountLight() {
        return lightList.size();
    }

    public Light getLight(int index) {
        return lightList.get(index);
    }

}
