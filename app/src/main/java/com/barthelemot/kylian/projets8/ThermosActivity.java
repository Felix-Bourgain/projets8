package com.barthelemot.kylian.projets8;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barthelemot.kylian.projets8.entity.Light;
import com.barthelemot.kylian.projets8.entity.Thermo;
import com.barthelemot.kylian.projets8.singleton.LightSingleton;
import com.barthelemot.kylian.projets8.singleton.ThermoSingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ThermosActivity extends AppCompatActivity {

    private final String URLDATABASE = "https://projets8-4d1a2-default-rtdb.europe-west1.firebasedatabase.app/";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thermo);

        //Notre base de données étant différente
        //de la base de données par défaut, nous devons passer son url en paramètre.
        mFirebaseDatabase = FirebaseDatabase.getInstance(URLDATABASE);
        myRef = mFirebaseDatabase.getReference();

        //Méthode d'initialisation des radiateurs pour le projet
        initializeThermoTest();

        mToolbar = (Toolbar) findViewById(R.id.toolBar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.recyclerView_Thermos);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ThermosActivity.ThermoAdapter();
        recyclerView.setAdapter(adapter);
    }

    Thermo thermo1;
    //Méthode d'initialisation des radiateurs pour le projet
    private void initializeThermoTest() {
        thermo1 = new Thermo(1, "Salon", (float) 24.5, (float) 25.0, (float) 25.0);

        //Récupération de la température programmé d'un radiateur, programmé lors de la fermeture de l'application (sur firebase)
        myRef.child("Thermos").child(String.valueOf(1)).child("tempProg").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                } else {
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    ThermoSingleton.getInstance().getThermo(0).setTempProg(Float.valueOf(task.getResult().getValue().toString()));
                    adapter.notifyDataSetChanged();
                    myRef.child("Thermos").child(Integer.toString(thermo1.getId())).setValue(thermo1);
                }
            }
        });
        //Récupération de la température réel d'un radiateur (sur firebase)
        myRef.child("Thermos").child(String.valueOf(1)).child("tempReel").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                } else {
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    ThermoSingleton.getInstance().getThermo(0).setTempReel(Float.valueOf(task.getResult().getValue().toString()));
                    adapter.notifyDataSetChanged();
                    myRef.child("Thermos").child(Integer.toString(thermo1.getId())).setValue(thermo1);
                }
            }
        });
        //Récupération de l'humidité réelle d'un radiateur (sur firebase)
        myRef.child("Thermos").child(String.valueOf(1)).child("humidity").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                } else {
                    Log.d("firebaseTest", String.valueOf(task.getResult().getValue()));
                    float val = Float.valueOf(task.getResult().getValue().toString());
                    ThermoSingleton.getInstance().getThermo(0).setHumidity(val);
                    adapter.notifyDataSetChanged();
                    myRef.child("Thermos").child(Integer.toString(thermo1.getId())).setValue(thermo1);
                }
            }
        });

        ArrayList<Thermo> al = new ArrayList<>();
        al.add(thermo1);
        ThermoSingleton.getInstance().setThermoList(al);
    }

    //ThermoAdapter pour adapter un radiateur à un item dans le recyclerView ainsi que chaque données
    public class ThermoAdapter extends RecyclerView.Adapter<ThermoAdapter.ViewHolder> {

        public ThermoAdapter() {
            super();
        }

        @NonNull
        @Override
        public ThermosActivity.ThermoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_thermo, parent, false);
            return new ThermosActivity.ThermoAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ThermosActivity.ThermoAdapter.ViewHolder holder, int position) {
            holder.setThermo(ThermoSingleton.getInstance().getThermo(position));
        }

        @Override
        public int getItemCount() {
            return ThermoSingleton.getInstance().getCountThermo();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            //private ImageView mThermoImg;
            private TextView mRoomName;
            private Button mBtnMoins;
            private Button mBtnPlus;
            private TextView mThermoReel;
            private TextView mThermoProg;
            private TextView mHumidity;

            public ViewHolder(View v) {
                super(v);
                mRoomName = v.findViewById(R.id.tv_roomName);
                mBtnMoins = v.findViewById(R.id.btn_moins);
                mBtnPlus = v.findViewById(R.id.btn_plus);
                mThermoReel = v.findViewById(R.id.tv_thermoActuel);
                mThermoProg = v.findViewById(R.id.tv_thermoVoulu);
                mHumidity = v.findViewById(R.id.tv_humidity);
            }

            public void setThermo(final Thermo thermo) {

                mRoomName.setText(thermo.getNomPiece());
                //Listener de la base de données vers l'application afin de mettre à jour en cas de changement en temps réel
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String tempReel = snapshot.child("Thermos").child(Integer.toString(thermo.getId())).child("tempReel").getValue().toString();
                        mThermoReel.setText("Réelle : " + tempReel + "°C");
                        ThermoSingleton.getInstance().getThermo(thermo.getId()-1).setTempReel(Float.valueOf(tempReel));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getBaseContext(), "Erreur de récupération des données", Toast.LENGTH_SHORT);
                        mThermoReel.setText("?");
                    }
                });
                //Listener de la base de données vers l'application afin de mettre à jour en cas de changement en temps réel
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String humidity = snapshot.child("Thermos").child(Integer.toString(thermo.getId())).child("humidity").getValue().toString();
                        mHumidity.setText("Humidité : " + humidity + "%");
                        ThermoSingleton.getInstance().getThermo(thermo.getId()-1).setTempReel(Float.valueOf(humidity));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getBaseContext(), "Erreur de récupération des données", Toast.LENGTH_SHORT);
                        mHumidity.setText("?");
                    }
                });

                mThermoProg.setText("Programmée : " + thermo.getTempProg() + "°C");

                mBtnMoins.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        thermo.baisser();
                        mThermoProg.setText("Programmée : " + thermo.getTempProg() + "°C");
                        updateProgTemp(thermo.getId(), thermo.getTempProg());
                    }
                });

                mBtnPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        thermo.monter();
                        mThermoProg.setText("Programmée : " + thermo.getTempProg() + "°C");
                        updateProgTemp(thermo.getId(), thermo.getTempProg());
                    }
                });
            }
        }
    }

    //Méthode de mise à jour de la température programmé d'un radiateur, de l'application vers Firebase
    public void updateProgTemp(int id, float newVal){
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/Thermos/"+id+"/tempProg/", newVal);
        myRef.updateChildren(childUpdates);
    }
}
