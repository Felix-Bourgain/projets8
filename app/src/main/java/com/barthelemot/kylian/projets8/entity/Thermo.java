package com.barthelemot.kylian.projets8.entity;

//Entité représentant un radiateur
public class Thermo {

    private int id;
    private String nomPiece;
    private float tempReel;
    private float tempProg;
    private float humidity;

    public Thermo(int id, String nomPiece, float tempReel, float tempProg, float humidity) {
        this.id = id;
        this.nomPiece = nomPiece;
        this.tempReel = tempReel;
        this.tempProg = tempProg;
        this.humidity = humidity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomPiece() {
        return nomPiece;
    }

    public void setNomPiece(String nomPiece) {
        this.nomPiece = nomPiece;
    }

    public float getTempReel() {
        return tempReel;
    }

    public void setTempReel(float tempReel) {
        this.tempReel = tempReel;
    }

    public float getTempProg() {
        return tempProg;
    }

    public void setTempProg(float tempProg) {
        this.tempProg = tempProg;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public void baisser(){
        if(tempProg > 0) {
            tempProg -= 0.5;
        }
    }

    public void monter(){
        if(tempProg < 30) {
            tempProg += 0.5;
        }
    }
}
