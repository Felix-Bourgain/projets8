package com.barthelemot.kylian.projets8;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    //Communication
    private final String URLDATABASE = "https://projets8-4d1a2-default-rtdb.europe-west1.firebasedatabase.app/";
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    private ImageButton btnLight;
    private ImageButton btnThermo;
    private ImageButton btnWindow;

    private ImageButton btnPower;

    private Switch switchDoor;

    private int powerStatus = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialisation entre FireBase et l'application
        FirebaseApp.initializeApp(this);

        mFirebaseDatabase = FirebaseDatabase.getInstance(URLDATABASE);
        myRef = mFirebaseDatabase.getReference();

        btnLight = findViewById(R.id.imgBtn_light);
        btnThermo = findViewById(R.id.imgBtn_thermo);
        btnWindow = findViewById(R.id.imgBtn_window);
        switchDoor = findViewById(R.id.switch_door);

        btnPower = findViewById(R.id.btn_power);

        //Mise en place des listener
        btnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToLightsPage();
            }
        });

        btnThermo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToThermosPage();
            }
        });

        btnWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToWindowsPage();
            }
        });

        //Récupération de l'état de la porte, si ouverture ou fermé lors de la fermeture de l'application
        actualDoorStatus();

        switchDoor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    myRef.child("Door").child("ouverture").setValue(1);
                    updateDoorStatus(1);
                }
                else {
                    myRef.child("Door").child("ouverture").setValue(0);
                    updateDoorStatus(0);
                }
            }
        });

        //Récupération du status réel de l'application (connecté ou déconnecté)
        actualPowerStatus();

        btnPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePowerStatus();
            }
        });
    }

    private void goToLightsPage() {
        Intent intent = new Intent(this, LightsActivity.class);
        startActivity(intent);
    }

    private void goToThermosPage() {
        Intent intent = new Intent(this, ThermosActivity.class);
        startActivity(intent);
    }

    private void goToWindowsPage() {
        Intent intent = new Intent(this, WindowsActivity.class);
        startActivity(intent);
    }

    //Mise à jour de Firebase pour la donnée de la porte (ouverte ou fermée)
    public void updateDoorStatus(int status){
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/Door/ouverture/", status);
        myRef.updateChildren(childUpdates);
    }

    //Récupération du status réel de la porte sur Firebase (ouverte ou fermée)
    public void actualDoorStatus() {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String valeur = snapshot.child("Door").child("ouverture").getValue().toString();
                if(valeur.equals("1")){ //Si 1 alors la porte est ouverte, sinon fermée.
                    switchDoor.setChecked(true);
                }
                else{
                    switchDoor.setChecked(false);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(), "Erreur de récupération des données", Toast.LENGTH_SHORT);
            }
        });
    }

    //Mise à jour du status de connexion de l'application.
    public void updatePowerStatus(){
        Map<String, Object> childUpdates = new HashMap<>();
        int status = 0;
        if(powerStatus == 0){
            status = 1;
            Toast.makeText(this,"Connexion...", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this,"Déconnexion...", Toast.LENGTH_LONG).show();
        }
        childUpdates.put("/Power/status/", status);
        myRef.updateChildren(childUpdates);
    }

    //Récupération de status réel de connexion de l'application.
    public void actualPowerStatus() {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String valeur = snapshot.child("Power").child("status").getValue().toString();
                if(valeur.equals("1")){
                    btnPower.setImageResource(R.drawable.ic_baseline_greenpower_settings_new_50);
                    powerStatus = 1;
                }
                else{
                    btnPower.setImageResource(R.drawable.ic_baseline_redpower_settings_new_50);
                    powerStatus = 0;
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getBaseContext(), "Erreur de récupération des données", Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}