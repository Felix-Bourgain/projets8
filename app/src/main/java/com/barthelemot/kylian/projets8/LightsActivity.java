package com.barthelemot.kylian.projets8;

import android.graphics.Color;
import android.os.Bundle;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barthelemot.kylian.projets8.entity.Light;
import com.barthelemot.kylian.projets8.singleton.LightSingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LightsActivity extends AppCompatActivity {

    private final String URLDATABASE = "https://projets8-4d1a2-default-rtdb.europe-west1.firebasedatabase.app/";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);

        //Notre base de données étant différente
        //de la base de données par défaut, nous devons passer son url en paramètre.
        mFirebaseDatabase = FirebaseDatabase.getInstance(URLDATABASE);
        myRef = mFirebaseDatabase.getReference();

        //Méthode d'initialisation des lumières
        initializeLightsTest();

        mToolbar = (Toolbar) findViewById(R.id.toolBar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.recyclerView_Lights);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new LightAdapter();
        recyclerView.setAdapter(adapter);
    }

    Light light1;
    Light light2;
    //Méthode d'initialisation des lumières pour le projet
    private void initializeLightsTest() {
        light1 = new Light(1, "Salon", 80,255, 255, 0);
        light2 = new Light(2, "Chambre", 50, 255, 255, 255);

        //Récupération de l'intensité programmé lors de la fermeture de l'application (sur firebase)
        myRef.child("Lights").child(String.valueOf(1)).child("intensity").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                }
                else {
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    light1.setIntensity(Integer.valueOf(task.getResult().getValue().toString()));
                    adapter.notifyDataSetChanged();
                    myRef.child("Lights").child(String.valueOf(LightSingleton.getInstance().getLight(0).getId())).setValue(LightSingleton.getInstance().getLight(0));
                }
            }
        });
        myRef.child("Lights").child(String.valueOf(2)).child("intensity").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                }
                else {
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    //LightSingleton.getInstance().getLightList().get(1).setIntensity(Integer.valueOf(task.getResult().getValue().toString()));
                    light2.setIntensity(Integer.valueOf(task.getResult().getValue().toString()));
                    adapter.notifyDataSetChanged();
                    myRef.child("Lights").child(String.valueOf(LightSingleton.getInstance().getLight(1).getId())).setValue(LightSingleton.getInstance().getLight(1));
                }
            }
        });

        ArrayList<Light> al = new ArrayList<>();
        al.add(light1);
        al.add(light2);

        LightSingleton.getInstance().setLightList(al);
    }

    //LightAdapter pour adapter une lumière à un item dans le recyclerView ainsi que chaque données
    public class LightAdapter extends RecyclerView.Adapter<LightAdapter.ViewHolder> {

        public LightAdapter() {
            super();
        }

        @NonNull
        @Override
        public LightAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_light, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull LightAdapter.ViewHolder holder, int position) {
            holder.setLight(LightSingleton.getInstance().getLight(position));
        }

        @Override
        public int getItemCount() {
            return LightSingleton.getInstance().getCountLight();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageButton mLightColor;
            private TextView mRoomName;
            private SeekBar mSeekBarIntensity;
            private TextView mIntensityValue;

            public ViewHolder(View v) {
                super(v);
                mLightColor = v.findViewById(R.id.imgBtn_colorLight);
                mRoomName = v.findViewById(R.id.tv_roomName);
                mSeekBarIntensity = v.findViewById(R.id.seekBar_intensity);
                mIntensityValue = v.findViewById(R.id.tv_intensity);
            }

            public void setLight(final Light light) {
                mLightColor.setOnClickListener((view) -> {
                    LightSingleton.getInstance().setCurrentLight(light);
                });
                mLightColor.setColorFilter(Color.rgb(light.getR(), light.getG(), light.getB()));
                mLightColor.setAlpha((float) (light.getIntensity()/100.0));
                mRoomName.setText(light.getNomPiece());
                mIntensityValue.setText(light.getIntensity()+"%");
                mSeekBarIntensity.setMax(100);
                mSeekBarIntensity.setMin(0);
                mSeekBarIntensity.incrementProgressBy(5);
                mSeekBarIntensity.setProgress(light.getIntensity());
                mSeekBarIntensity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        mIntensityValue.setText(i+"%");
                        mLightColor.setAlpha((float) (i/100.0));
                        Log.d("TestL", i+"% avec " + mLightColor.getAlpha());
                        updateIntensity(light.getId(), i);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });
            }
        }
    }

    //Méthode de mise à jour de l'intensité d'une lumière, de l'application vers Firebase
    public void updateIntensity(int id, float newVal){
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/Lights/"+id+"/intensity/", newVal);
        myRef.updateChildren(childUpdates);
    }

}
