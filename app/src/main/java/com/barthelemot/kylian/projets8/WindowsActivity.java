package com.barthelemot.kylian.projets8;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barthelemot.kylian.projets8.entity.Window;
import com.barthelemot.kylian.projets8.singleton.WindowSingleton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WindowsActivity extends AppCompatActivity {

    private final String URLDATABASE = "https://projets8-4d1a2-default-rtdb.europe-west1.firebasedatabase.app/";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_window);

        //Notre base de données étant différente
        //de la base de données par défaut, nous devons passer son url en paramètre.
        mFirebaseDatabase = FirebaseDatabase.getInstance(URLDATABASE);
        myRef = mFirebaseDatabase.getReference();

        //Méthode d'initialisation des fenêtres
        initializeWidowsTest();

        mToolbar = (Toolbar) findViewById(R.id.toolBar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.recyclerView_Windows);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new WindowAdapter();
        recyclerView.setAdapter(adapter);
    }

    Window window1;
    //Méthode d'initialisation des fenêtres pour le projet
    private void initializeWidowsTest() {
        window1 = new Window(1, "Salon", 100);

        //Récupération de l'ouverture de la fenêtre programmé lors de la fermeture de l'application (sur firebase)
        myRef.child("Windows").child(String.valueOf(1)).child("ouverture").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if(!task.isSuccessful()) {
                    Log.e("firebase", "Error getting data", task.getException());
                }
                else {
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));
                    WindowSingleton.getInstance().getListWindow().get(0).setOuverture(Integer.valueOf(task.getResult().getValue().toString()));
                    adapter.notifyDataSetChanged();
                    myRef.child("Windows").child(Integer.toString(window1.getId())).setValue(window1);
                }
            }
        });

        ArrayList<Window> al = new ArrayList<>();
        al.add(window1);
        WindowSingleton.getInstance().setWindowList(al);
    }

    //WindowAdapter pour adapter une fenêtre à un item dans le recyclerView ainsi que chaque données
    public class WindowAdapter extends RecyclerView.Adapter<WindowsActivity.WindowAdapter.ViewHolder> {

        public WindowAdapter() {
            super();
        }

        @NonNull
        @Override
        public WindowsActivity.WindowAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_window, parent, false);
            return new WindowsActivity.WindowAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull WindowsActivity.WindowAdapter.ViewHolder holder, int position) {
            holder.setWindow(WindowSingleton.getInstance().getWindow(position));
        }

        @Override
        public int getItemCount() {
            return WindowSingleton.getInstance().getCountWindow();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView mImageWindow;
            private TextView mRoomName;
            private SeekBar mSeekBarIntensity;
            private TextView mOuvertureValue;

            public ViewHolder(View v) {
                super(v);
                mImageWindow = v.findViewById(R.id.img_windows);
                mRoomName = v.findViewById(R.id.tv_roomName);
                mSeekBarIntensity = v.findViewById(R.id.seekBar_intensity);
                mOuvertureValue = v.findViewById(R.id.tv_intensity);
            }

            public void setWindow(final Window window) {
                mImageWindow.setAlpha((float) (window.getOuverture()/100.0));
                mRoomName.setText(window.getNomPiece());
                mOuvertureValue.setText(window.getOuverture()+"%");
                mSeekBarIntensity.setMax(100);
                mSeekBarIntensity.setMin(0);
                mSeekBarIntensity.incrementProgressBy(5);
                mSeekBarIntensity.setProgress(window.getOuverture());
                mSeekBarIntensity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        mOuvertureValue.setText(i+"%");
                        mImageWindow.setAlpha((float) (i/100.0));
                        Log.d("TestL", i+"% avec " + mImageWindow.getAlpha());
                        updateOuverture(window.getId(), i);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {}

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {}
                });
            }
        }
    }

    //Méthode de mise à jour de l'ouverture d'une fenêtre, de l'application vers Firebase
    public void updateOuverture(int id, float newVal){
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/Windows/"+id+"/ouverture/", newVal);
        myRef.updateChildren(childUpdates);
    }
}
